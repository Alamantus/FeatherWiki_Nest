# Feather Wiki Nest

This project is planned to create an easily self-hostable server that makes it possible to save a [Feather Wiki](https://codeberg.org/Alamantus/FeatherWiki) directly to the server after making changes instead of needing to save the file locally before uploading it to your web host.

## Plans & Ideas

- Maybe write a PHP script for people with traditional web hosts that don't allow script execution

## How to Use

Download the `featherwiki.nest` file (not ready to download just yet) and execute it in the
command line/terminal. (If it's not already executable, use `chmod +x featherwiki.nest` if you're
in a UNIX-like system, i.e. Linux or MacOS.) This executable is a [redbean.com](https://redbean.dev)
webserver that has been modified specifically for Feather Wiki. Then you just run `./featherwiki.nest`
in Linux or `bash -c './featherwiki.nest'` in MacOS to start the server! Once running, visit http://localhost:8080
to see your empty Feather Wiki.

But that's not very exciting, is it? Luckily, Feather Wiki Nest uses the Tern build of Feather Wiki, which
can save directly to the server instead of requiring you to download the result like normal!
If you specify a username and password when running the executable, you can enable logging in from anywhere
so you can save directly to the server. Add `username=YourArbitraryUsername password=YourStrongPassword` to your
command, and you will be able to access `http://localhost:8080/login.lua` to enable the "Save to Server"
button from the Feather Wiki site. Note that this button appears anyway, but you must be logged in for it
to actually function.

### Good to Know

Redbean.com servers are actually just magic `.zip` archives that can be extracted just like any other zip file!
By default in Linux, your Feather Wiki is saved directly into the `featherwiki.nest` file, and it can be
extracted with any zip file extractor if you need to use it elsewhere. If you're having trouble, just rename
`featherwiki.nest` to `featherwiki.zip` and your computer should see what it needs to do from there.

On non-Linux hosts, your Feather Wiki file will be saved to the same directory outside of the `featherwiki.nest`
file. You can choose to do this on Linux as well by adding `-D . file=featherwiki.html` to the execution.

You can replace the favicon for your server by zipping a new file called `favicon.ico` into the root of
the `featherwiki.nest` zip file using InfoZIP (the `zip` command in Linux, which for example you can get
with `apt install zip` on Ubuntu/Debian).

## Development

Build using `build.sh` in a UNIX-like system (eg. Linux or MacOS). Make it executable
with `chmod +x build.sh` and run it with ./build.sh. In MacOS, you have to specify
bash instead of zsh with `bash -c ./build.sh`. This will download redbean.com and an
empty copy of Feather Wiki's Tern build plus the Feather Wiki favicon into a `vendor`
folder and then zip all the code into a copy of `vendor/redbean.com` that ends up in
`dist/featherwiki.nest`. Run `build.sh` again when you make changes, but be aware that
this deletes the `dist` directory and rebuilds from scratch every time.

Maybe I'll make a PowerShell or CMD version of the build script for Windows, but that
sounds annoying so we'll see.
