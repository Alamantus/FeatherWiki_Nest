#!/bin/sh
rm -r ./dist
mkdir ./dist
if [ ! -d "./vendor" ]; then
  mkdir ./vendor
fi
if [ ! -f "./vendor/redbean.com" ]; then
  curl https://redbean.dev/redbean-tiny-2.0.15.com > ./vendor/redbean.com
fi
if [ ! -f "./vendor/featherwiki.html" ]; then
  curl https://feather.wiki/builds/FeatherWiki_Tern.html > ./vendor/featherwiki.html
  curl https://feather.wiki/favicon.ico > ./vendor/favicon.ico
fi

cp ./vendor/redbean.com ./dist/featherwiki.zip
# Remove unnecessary files
zip -d ./dist/featherwiki.zip help.txt favicon.ico redbean.png

cd ./vendor
zip ../dist/featherwiki.zip featherwiki.html favicon.ico
cd ../src
zip ../dist/featherwiki.zip .args .init.lua *.lua .lua/*
cd ..
mv ./dist/featherwiki.zip ./dist/featherwiki.nest
chmod +x ./dist/featherwiki.nest
