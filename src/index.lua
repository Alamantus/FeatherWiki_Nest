require 'constants'

method = GetMethod()
path = GetPath()

if method == 'GET' then
  if USE_FILESYSTEM then
    Write(Slurp(WIKI_PATH))
  else
    ServeAsset(WIKI_PATH)
  end
elseif method == 'HEAD' then
  SetStatus(200)
  length = #(LoadAsset(WIKI_PATH))
  SetHeader('Content-Length', tostring(length))
elseif method == 'OPTIONS' then
  SetStatus(200)
  SetHeader('allow', 'GET,HEAD,POST,OPTIONS,CONNECT,PUT,DAV,dav')
  SetHeader('x-api-access-type', 'file')
  SetHeader('dav', 'put')
elseif method == 'PUT' then
  local cookie = GetCookie('login')
  length = tonumber(GetHeader('Content-Length'))
  if cookie and LOGIN_HASH ~=nil and DecodeBase64(cookie) == LOGIN_HASH then
    body = GetBody()
    if USE_FILESYSTEM then
      Barf(WIKI_PATH, body)
    else
      StoreAsset(WIKI_PATH, body)
    end
    SetStatus(201, 'Updated Successfully')
  else
    SetStatus(401)
  end
end

