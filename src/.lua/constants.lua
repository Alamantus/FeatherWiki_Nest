local path = require 'path'

MAX_PAYLOAD_SIZE = 5000000
EXECUTE_DIR = path.dirname(arg[-1])
WIKI_PATH = 'featherwiki.html'
USE_FILESYSTEM = false
LOGIN_HASH = nil

local argTable = {}
for i,a in ipairs(arg) do
  if not string.find(a, '=') then
  else
    for k,v in string.gmatch(a, '(%w+)=(.+)') do
      argTable[k] = v
    end
  end
end

if argTable.max_payload_size ~= nil then
  MAX_PAYLOAD_SIZE = tonumber(argTable.max_payload_size)
end

if (GetHostOs() ~= 'LINUX' or argTable.file ~=nil) then
  USE_FILESYSTEM = true
  WIKI_PATH = path.join(EXECUTE_DIR, argTable.file)
end

if (argTable.username ~= nil and argTable.password ~= nil) then
  local argon2 = require 'argon2'
  local combo = argTable.username .. ' ' .. argTable.password
  local salt = argTable.username
  if #salt < 8 then
    salt = salt .. string.rep('.', 8 - #salt)
  end
  LOGIN_HASH = argon2.hash_encoded(combo, salt, {
    m_cost = 65536,
    hash_len = 32,
    parallelism = 4,
    t_cost = 2,
  })
end
