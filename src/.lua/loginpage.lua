require 'constants'

SetHeader('Content-Type', 'text/html')
Write('<!doctype html><html><head><meta charset="utf-8"><meta name="viewport" content="width=device-width,initial-scale=1"><title>Log In</title></head><body>')
if not LOGIN_HASH then
  Write('<h1>No credentials set - Editing is disabled</h1>')
--  Write('<h1>Set Login Credentials</h1>')
--  Write('<p>There are no credentials set on this server. Set them now to enable saving changes in the future.</p>')
--  Write('<form method="POST"><label>Username <input name="username"></label><br><label>Password <input type="password" name="password"></label><br><button type="submit">Submit</button></form>')
else
  Write('<h1>Log In</h1>')
  Write('<p>Enter your username and password to enable saving changes.</p>')
  Write('<form method="POST"><label>Username <input name="username"></label><br><label>Password <input type="password" name="password"></label><br><button type="submit">Submit</button></form>')
end
Write('</body></html>')
