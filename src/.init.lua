-- special script called by main redbean process at startup
HidePath('/usr/share/zoneinfo/')
HidePath('/usr/share/ssl/')

require 'constants'

ProgramMaxPayloadSize(MAX_PAYLOAD_SIZE)

if USE_FILESYSTEM then
  print('Using filesystem instead of storing within ' .. arg[-1])
  ProgramDirectory(EXECUTE_DIR)
  local path = require 'path'
  if not path.exists(WIKI_PATH) then
    Barf(WIKI_PATH, LoadAsset('featherwiki.html'))
  end
end
