require 'constants'

method = GetMethod()

if method == 'POST' then
  local username = GetParam('username')
  local password = GetParam('password')
  local combo = username .. ' ' .. password
  if LOGIN_HASH ~= nil then
    local argon2 = require 'argon2'
    if argon2.verify(LOGIN_HASH, combo) then
      SetStatus(202, 'Logged In Successfully')
      SetCookie('login', EncodeBase64(LOGIN_HASH))
      Write('<h1>You are logged in!</h1>')
      Write('<p><a href="./">Go to Feather Wiki</a></p>')
    else
      SetStatus(401, 'Incorrect Credentials')
      Write('<h1>Incorrect Credentials</h1>')
      Write('<p><button onclick="history.back()">Go Back</button></p>')
    end
  end
else
  dofile('/zip/.lua/loginpage.lua')
end
